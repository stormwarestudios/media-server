// TODO: Use yargs to parse command-line options

const cluster = require('cluster');
const os = require('os');
const http = require('http');
const url = require('url');
const fs = require('fs');
const path = require('path');
const port = process.argv[2] || 9000;
const webroot = path.join(process.cwd(), process.argv[3] || 'www');

function createServer() {
  http.createServer(function (req, res) {
    console.log(`${process.pid} ${req.method} ${req.url}`);

    // parse URL
    const parsedUrl = url.parse(req.url);
    // extract URL path
    let pathname = `.${parsedUrl.pathname}`;
    // based on the URL path, extract the file extension. e.g. .js, .doc, ...
    const ext = path.parse(pathname).ext;
    // maps file extention to MIME types
    const map = {
      '.ico': 'image/x-icon',
      '.html': 'text/html',
      '.js': 'text/javascript',
      '.json': 'application/json',
      '.css': 'text/css',
      '.png': 'image/png',
      '.jpg': 'image/jpeg',
      '.wav': 'audio/wav',
      '.mp3': 'audio/mpeg',
      '.svg': 'image/svg+xml',
      '.pdf': 'application/pdf',
      '.doc': 'application/msword',
      '.fx' : 'application/fx',
      '.babylon': 'application/babylon',
      '.babylonmeshdata': 'application/babylonmeshdata'
    };


    const requestedFile = path.join(webroot, pathname);

    console.log(`Looking for file ${requestedFile}`);

    fs.exists(requestedFile, function (exist) {
      if(!exist) {
        // if the file is not found, return 404
        res.statusCode = 404;
        res.end(`File ${requestedFile} not found!`);
        return;
      }

      // if is a directory search for index file matching the extention
      fs.stat(requestedFile, (err, stats) => {
        if (err) {
          res.statusCode = 500;
          res.end(`Invalid request`);
          return;
        }

        if (stats.isDirectory()) {
          res.statusCode = 500;
          res.end(`Invalid request`);
          return;
        }

        res.setHeader('Content-Type', map[ext] || 'text/plain');

        const rstream = fs.createReadStream(requestedFile);

        rstream.pipe(res).on('error', (err) => {
          res.statusCode = 500;
          res.end(`Error getting the file: ${err}.`);
        });
      });
    });
  }).listen(parseInt(port), '127.0.0.1');
}

function main() {
  if (cluster.isMaster) {
    const numWorkers = os.cpus().length;
    for (let i = 0; i < numWorkers; i++) {
      cluster.fork();
    }

    cluster.on('online', function(worker) {
      console.log('Worker ' + worker.process.pid + ' is online');
    });

    cluster.on('exit', function(worker, code, signal) {
      console.log('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
      console.log('Starting a new worker');
      cluster.fork();
    });
  } else {
    createServer();
  }
}

main();
